[Advent of Code 2022](https://adventofcode.com/2022) solultions implemented using my toy language [knife](https://gitlab.com/rusty_knife/knifelang).

The compiler has changed quite a bit over the course of me solving these puzzles, so it's quite likely a lot of them won't work by the time AoC is over. Same goes for the "standard library" found in [std](std).
